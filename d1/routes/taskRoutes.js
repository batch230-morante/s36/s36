const express = require("express");
// express.Router() method allows access to HTTP methods
const router = express.Router();
const taskController = require("../controllers/taskController");

// Create - task routes
router.post("/addTask", taskController.createTaskController);


// Get all tasks
router.get("/allTasks", taskController.getAllTasksController);


// delete a task
router.delete("/deleteTask/:taskId", taskController.deleteTaskController)



// Get specific task
router.get("/:taskId", taskController.getSpecificTaskController)


// Update Task status
router.put("/updateTaskStatus/:taskId/complete", taskController.updateTaskStatusController)



router.put("/:id/archive", (req, res) => {
	taskController.changeNameToArchive(req.params.id).then(resultFromController => res.send(resultFromController));
})



router.patch("/updateTask/:taskId", taskController.updateTaskNameController);

module.exports = router;

